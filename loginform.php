<?php
    require_once("headerpage.php");
?>

 <!-- Breadcrumbs-->
 <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">HOME</a>
        </li>
        <li class="breadcrumb-item active">Login Form</li>
 </ol>
      <div class="row">
        <div class="col-6">
        <form action="proseslogin.php" method="post">
            <h2>LOGIN</h2>

           
  <div class="form-group">
    <label for="username">Username :</label>
    <input type="text" class="form-control" name="username">
  </div>
  <div class="form-group">
    <label for="pass">Password :</label>
    <input type="password" class="form-control" name="sandi">
  </div>
  <div class="checkbox">
    <label><input type="checkbox"> Remember me</label>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>

        </div>
      </div>

       <?php
                if(isset($_GET["pesan"]) && $_GET["pesan"]!=""){
                    echo "<span class='alert alert-success'>".$_GET["pesan"]."</span>";
                }
            ?>

<?php
    require_once("footerpage.php");
?>