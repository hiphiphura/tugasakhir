<?php
    session_start();

    $username=$_POST["username"];
    $pass=$_POST["sandi"];
    $hashpass=md5($pass);

    require_once("koneksi.php");

    try{
        $stmt = $conn->prepare('select username,pass,status from pengguna where username=? and pass=?');
        $stmt->bind_param("ss", $username, $hashpass);
        $stmt->execute();
        $stmt->bind_result($user, $sandi,$aturan);
    
        //cek jika ada data pengguna
        while($stmt->fetch()) {
            $isloginsuccess = true;
            $_SESSION["username"] = $user;
            $_SESSION["status"] = $aturan;
            
        }

        if($isloginsuccess==true){
            header("Location: /tugasakhir/index.php");
        }
        else {
            $pesan = "Username/Password yang anda masukan tidak sesuai...";
            header("Location: /tugasakhir/loginform.php?pesan=$pesan");
        }
    }
    catch(Exception $e){
        $error = $e->getMessage();
    }
        $stmt->close();
        $conn->close();
    
    