<?php
    require_once("headerpage.php");
?>

 <!-- Breadcrumbs-->
 <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">HOME</a>
        </li>
        <li class="breadcrumb-item active">Registration Page</li>
 </ol>
      <div class="row">
        <div class="col-6">
        <form action="prosesregistrasi.php" method="post">
            <h2>Reistration Form</h2>
            <?php
                if(isset($_GET["pesan"]) && $_GET["pesan"]!=""){
                    echo "<span class='alert alert-success'>".$_GET["pesan"]."</span>";
                }
            ?>
  <div class="form-group">
    <label for="username">Username :</label>
    <input type="text" class="form-control" name="username">
  </div>
  <div class="form-group">
    <label for="pass">Password :</label>
    <input type="password" class="form-control" name="sandi">
  </div>
  <div class="form-group">
    <label for="repass">Re-Password :</label>
    <input type="password" class="form-control" name="sandi1">
  </div>
  <div class="form-group">
    <label for="nama">Name :</label>
    <input type="text" class="form-control" name="nama">
  </div>
  <div class="form-group">
    <label for="email">Email :</label>
    <input type="email" class="form-control" name="email">
  </div>
  <div class="form-group">
    <label for="telp">Phone :</label>
    <input type="text" class="form-control" name="telp">
  </div>
  <div class="checkbox">
    <label><input type="checkbox"> Remember me</label>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>

        </div>
      </div>

<?php
    require_once("footerpage.php");
?>