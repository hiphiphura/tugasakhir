<?php

//if(!isset($_SESSION["username"])){
  //  header("Location: /PROGWEBB1/loginform.php");
//}
    require_once("headerpage.php");
?>

 <!-- Breadcrumbs-->
 <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.php">HOME</a>
        </li>
        <li class="breadcrumb-item active">Tambah Produk</li>
 </ol>
      <div class="row">
        <div class="col-6">
          <h1>Tambah Produk</h1>
          <form action="prosestambahproduk.php" method="post" enctype="multipart/form-data">
          
          <div class="form-group">
                <label for="NamaProduk">Nama Produk:</label>
                <input type="text" class="form-control" name="NamaProduk">
            </div>
            <div class="form-group">
                <label for="Deskripsi">Deskripsi:</label>
                <textarea class="form-control" name="Deskripsi" rows="5"></textarea>
            </div>
            <div class="form-group">
                <label for="Jumlah">Jumlah:</label>
                <input type="number" class="form-control" name="Jumlah">
            </div>
            
            <div class="form-group">
                <label for="HargaJual">Harga Jual:</label>
                <input type="number" class="form-control" name="HargaJual">
            </div>
            <div class="form-group">
                <label for="fileToUpload">Gambar:</label>
                <input type="file" name="fileToUpload">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
          </form>
          
          
        </div>
      </div>

<?php
    require_once("footerpage.php");
?>