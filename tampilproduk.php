<?php
    require_once("headerpage.php");
?>

 <?php
    $servername="localhost";
    $username="root";
    $password="";
    $dbname="toko_online";

    $conn = new mysqli($servername,$username,
    $password,$dbname);

if($conn->connect_error){
    die("Koneksi gagal");
}

echo "<h3>Daftar Produk</h3>";

// $sql = "select * from produk";
// $result = $conn->query($sql);

// if($result->num_rows>0){
// echo "<table border='1'>";
// echo "<tr><th>ProductID</th>
// <th>NamaProduk</th><th>Deskripsi</th>
// <th>Jumlah</th><th>HargaJual</th>
// <th>Gambar</th>
// </tr>";
//     while($row=$result->fetch_assoc()){
//         echo "<tr><td>".$row["ProductID"]."</td>".
//         "<td>".$row["NamaProduct"]."</td>".
//         "<td>".$row["Deskripsi"]."</td>".
//         "<td>".$row["Jumlah"]."</td>".
//         "<td>".$row["HargaJual"]."</td>".
//         "<td> <img src='./images/".$row["Gambar"]."' width = 25%></td></tr>";
//     }
// echo "</table>";
// } else {
//     echo "Tidak dapat menampilkan record";
// }

// $conn->close();
 ?>

<div class="row">
  
  <?php 
      $limit = 2;  
      if (isset($_GET["page"])) { 
          $page  = $_GET["page"]; 
      } else { 
          $page=1; 
      };
      
      $start_from = ($page-1) * $limit;  
      $sql = "SELECT id_produk,nama_prod,descript,jumlah,harga,gambar FROM produk ORDER BY nama_prod ASC LIMIT ?,?";  
      $stmt = $conn->prepare($sql);
      $stmt->bind_param("ii", $start_from, $limit);
      $stmt->execute();
      $stmt->bind_result($id_produk,$nama_prod,$descript,$jumlah,$harga,$gambar);
  ?>
  <?php while($stmt->fetch()) { ?>
      <div class="col-md-6">
          <div class="row">
              <div class="col-md-3">
                  <img src="images/<?=$gambar?>" width="100" />
              </div>
              <div class="col-md-8">
                  <strong><?=$nama_prod?></strong>
                  <p><?=$descript?></p>
                  Harga Jual: <strong>Rp <?=$harga?></strong><br/>
                  <a href="prosestambahcart.php?id_produk=<?=$id_produk?>" class='btn btn-success btn-sm'>Add To Cart</a>
              </div>
          </div>
      </div>
  <?php } ?>
  
</div>
<div class="row">
  <?php 
      $stmt->close();
      
      $sqlpaging = "select count(id_produk) from produk";  
      $result = $conn->query($sqlpaging);
      $row = $result->fetch_array();  
      $total_records = $row['count(id_produk)'];  
      $total_pages = ceil($total_records / $limit);  
      $pagLink = "<ul class='pagination'>";  
      for ($i=1; $i<=$total_pages; $i++) {  
                   $pagLink .= "<li><a href='tampilproduk.php?page=".$i."'>".$i."</a></li>";  
      };  
      echo $pagLink . "</ul>";  
      $conn->close();
  ?>
  </div>

<?php
    require_once("footerpage.php");
?>