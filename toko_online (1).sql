-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2018 at 08:36 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id_cart` int(3) NOT NULL,
  `id_produk` int(3) NOT NULL,
  `no_invoice` int(3) NOT NULL,
  `jumlah` int(3) NOT NULL,
  `harga` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id_cart`, `id_produk`, `no_invoice`, `jumlah`, `harga`) VALUES
(1, 1, 1, 5, 20000),
(2, 2, 1, 1, 20000),
(3, 1, 2, 2, 20000),
(4, 2, 2, 1, 20000);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `no_invoice` int(3) NOT NULL,
  `username` varchar(100) NOT NULL,
  `tot_bayar` int(11) NOT NULL,
  `status_bayar` varchar(5) NOT NULL DEFAULT 'BELUM'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`no_invoice`, `username`, `tot_bayar`, `status_bayar`) VALUES
(1, 'pembeli', 0, 'BELUM'),
(2, 'pembeli', 0, 'LUNAS'),
(3, 'admin', 0, 'BELUM');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `username` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `no_telp` varchar(30) NOT NULL,
  `status` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`username`, `pass`, `nama`, `email`, `no_telp`, `status`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3', 'amanAdmin', 'amansejahtera1@gmail.com', '0987654322', 'admin'),
('pembeli', 'a9f8bbb8cb84375f241ce3b9da6219a1', 'pembeli', 'pembeli@gmail.com', '06378293', 'member');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(3) NOT NULL,
  `nama_prod` varchar(50) NOT NULL,
  `descript` text NOT NULL,
  `jumlah` int(3) NOT NULL,
  `harga` int(9) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_prod`, `descript`, `jumlah`, `harga`, `gambar`) VALUES
(1, 'Baju 1', 'blablabla', 13, 20000, '5afda32c921e9ANIMODE_BON VOYAGE BLACK (UNISEX)_100K 1.jpg'),
(2, 'Baju 2', 'blablabla', 13, 25000, '5afda350e82f2ANIMODE_BON VOYAGE BLACK (UNISEX)_100K.jpg'),
(3, 'baju lagi', 'dasda', 0, 10000, '5afec811c27d5ANIMODE_BON VOYAGE NAVY (woMEN)_100K 1.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_cart`),
  ADD KEY `id_cart` (`id_cart`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`no_invoice`),
  ADD KEY `no_invoice` (`no_invoice`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`username`),
  ADD KEY `username` (`username`),
  ADD KEY `username_2` (`username`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `id_produk` (`id_produk`),
  ADD KEY `id_produk_2` (`id_produk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id_cart` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `no_invoice` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
