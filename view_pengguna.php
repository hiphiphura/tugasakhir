<?php
    require_once("headerpage.php");
?>

 <?php
    $servername="localhost";
    $username="root";
    $password="";
    $dbname="ukdwstoredb";

    $conn = new mysqli($servername,$username,
    $password,$dbname);

if($conn->connect_error){
    die("Koneksi gagal");
}

echo "<h3>Daftar Pengguna</h3>";

$sql = "select * from pengguna order by nama asc";
$result = $conn->query($sql);

if($result->num_rows>0){
echo "<table border='1'>";
echo "<tr><th>username</th><th>nama</th><th>email</th><th>tlp</th></tr>";
    while($row=$result->fetch_assoc()){
        echo "<tr><td>".$row["username"]."</td>".
        "<td>".$row["nama"]."</td>"."<td>".$row["email"]."</td>"."<td>".$row["telp"]."</td>"."</tr>";
    }
echo "</table>";
} else {
    echo "Tidak dapat menampilkan record";
}

$conn->close();
 ?>

<?php
    require_once("footerpage.php");
?>